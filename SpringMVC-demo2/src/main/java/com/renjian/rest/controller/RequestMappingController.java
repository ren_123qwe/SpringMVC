package com.renjian.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
//@RequestMapping("hello")
public class RequestMappingController {

    @RequestMapping(
            value = {"/testRequestMapping","/test"},
            method = {RequestMethod.GET,RequestMethod.POST},
//            params = "username",表示必须携带username，username=“admin”，
//            表示参数username必须等于admin
            params = "username=admin"
            )
    public String success(){

        return "success";
    }

    @GetMapping("/target")
    public String target(){
        return "test";
    }

    @RequestMapping(value = "/testPut",method = {RequestMethod.PUT})
    public String testPut(){
        return "success";
    }
    @RequestMapping(value = {"/testParams"}
    ,method = {RequestMethod.GET,RequestMethod.POST},
     params = {"username=admin"},headers = {"Referer=http://localhost:8080/SpringMVC/"}
    )
    public String test(){
        return "success";
    }

//    @RequestMapping("/t?t/testAnt")
//@RequestMapping("/t*t/testAnt")
@RequestMapping("/t**t/testAnt")
    public String testAnt(){

        return "success";
    }

//    {}当前的括号就表示后边的通过reful方式传入的参数
//@PathVariable("id")该注解只能修饰形参，让SpringMVC获得到他的值
    @RequestMapping("/testPath/{id}/{username}")
    public String testPath(@PathVariable("id")Integer id,@PathVariable String username){

        System.out.println("id:"+id+"username: "+username);
        return "success";
    }
}
