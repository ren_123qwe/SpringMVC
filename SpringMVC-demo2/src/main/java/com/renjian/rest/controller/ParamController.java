package com.renjian.rest.controller;

import com.renjian.rest.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;

//专门处理获取请求参数参数
@Controller
public class ParamController {

    @RequestMapping("/testSerlevtAPI")
//    如果该参数的形参是HttpServletRequest，
//    前端控制器就会将他获取的本次本次请求的servlet赋值给request
//    也就是说，当前的request可以直接使用表示当前请求
    public String testServletAPI(HttpServletRequest request){
        HttpSession session = request.getSession();


        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println(username+password);
        return "success";
    }

        @RequestMapping("/testParam")
//    只要当形参的名称与参数的名称一直，Dispatcher Servlet前端控制器就可以直接赋值
    public String testParam(
//            @RequestParam(value="user_name",required=true)中
//            参数就是前端页面需要获取参数的对应的name,required的值为true，表示必须传入该值，没有就报错，false可以不必传入，默认为null,
//            defaultValue默认值，当没有传入当user_name的值时或者当前输入值为空时，也会展现默认值即为默认值hehe
            @RequestParam(value="user_name",required = true,defaultValue = "hehe") String username,
            String password,
            String[] hobby,
            @RequestHeader(value="Host") String host,
            @CookieValue("JSESSIONID") String cookies){
//     如果遇到多个参数同名，就是复选框等情况，
//     可以设置一个同名的数组进行接收，也可以使用一个字符串类型，
//     如果使用的是一个字符串，能正常就收，各个参数之间用逗号隔开
        System.out.println("username: "+username+","+"password: "+  password+","+"hobby:"+
                Arrays.toString(hobby)+"host: "+host);
        System.out.println("cookie: "+cookies);
        return "success";

    }
    @RequestMapping("/testBean")
    public String testBean(User user){
        System.out.println(user);
        return "success";
    }


}
