package com.renjian.mvc.dao.impl;

import com.renjian.mvc.pojo.User;

import java.util.List;

public interface UserDao {
//    查看全部人的信息
    public List<User> findAll();

    //添加数据
    public void add(User user);

    //修改数据
    public void update(User user);

    //按id删除数据
    public void delete(Integer id);

//    根据id查询出相关用户信息
    public User findone(Integer id);
}
