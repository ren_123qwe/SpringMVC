package com.renjian.mvc.dao.impl.impl;

import com.renjian.mvc.dao.impl.UserDao;
import com.renjian.mvc.pojo.User;
import jdk.nashorn.internal.ir.CallNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserdaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<User> findAll() {
        String sql = "select *from user ";
        List<User> users = jdbcTemplate.query(sql, new BeanPropertyRowMapper<User>(User.class));
        System.out.println("返回一个集合："+users);
        return users;
    }

    public void add(User user) {
        String sql="insert into user values(?,?,?,?)";
        int update = jdbcTemplate.update(sql, user.getId(), user.getUsername(), user.getSex(), user.getEmail());
        System.out.println(update);

    }

    public void update(User user) {
        String sql="update user set username=?,sex=?,email=? where id=?";
        int update = jdbcTemplate.update(sql, user.getUsername(), user.getSex(), user.getEmail(), user.getId());
        System.out.println(update);
    }

    public void delete(Integer id) {
        String sql="delete from user where id=?";
        int update = jdbcTemplate.update(sql, id);
        System.out.println(update);
    }

    public User findone(Integer id) {
        String sql = "select * from user where id = ?";
        User user = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), id);
        System.out.println("查询返回一个对象："+user);
        return user;
    }
}
