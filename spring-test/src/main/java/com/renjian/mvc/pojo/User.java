package com.renjian.mvc.pojo;


import org.springframework.stereotype.Component;

@Component
public class User {
    private Integer id;
//    1代表男，0代表女
    private Integer sex;

    private String email;

    private String username;

    public User(Integer id, Integer sex, String email, String username) {
        this.id = id;
        this.sex = sex;
        this.email = email;
        this.username = username;
    }

    public User() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public Integer getSex() {
        return sex;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", sex=" + sex +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
