package com.renjian.mvc.controller;

import com.renjian.mvc.pojo.User;
import com.renjian.mvc.web.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    //    查询全部信息
    @RequestMapping(value = "/user")
    public String findUsers(Model model) {
        List<User> all = userService.findAll();
        model.addAttribute("users", all);
        return "user_info";
    }

    //    实现添加
    @RequestMapping(value = "/user", method = {RequestMethod.POST})
    public String addUsers(User user) {
        userService.add(user);
        return "redirect:/user";
    }

    //    实现数据回显
    @GetMapping(value = "/user/{id}")
    public String updateUser(@PathVariable Integer id, Model model) {
        User user = userService.findone(id);
        System.out.println("实现数据回显  "+user);
        model.addAttribute("user", user);
        return "update_info";
    }

//    实现数据的修改
    @RequestMapping(value = "/user/{id}", method = {RequestMethod.PUT})
    public String doupdateUser(User user,@PathVariable Integer id) {
        System.out.println("实现数据修改  "+user.getId());
        userService.update(user);
        return "redirect:/user";
    }

    @DeleteMapping(value = "/user/{id}")
    public String deleteByID(@PathVariable Integer id) {
        userService.delete(id);
        return "redirect:/user";
    }
}
