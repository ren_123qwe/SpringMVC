package com.renjian.mvc.web.impl;

import com.renjian.mvc.pojo.User;

import java.util.List;

public interface UserService {
    public List<User> findAll();

    //添加数据
    public void add(User user);

    //修改数据
    public void update(User user);

    //按id删除数据
    public void delete(Integer id);

    public User findone(Integer id);

}
