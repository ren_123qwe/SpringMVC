package com.renjian.mvc.web;

import com.renjian.mvc.dao.impl.UserDao;
import com.renjian.mvc.pojo.User;
import com.renjian.mvc.web.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    public List<User> findAll(){
        return userDao.findAll();
    }

    public void add(User user) {
        userDao.add(user);
    }

    public void update(User user) {
        userDao.update(user);

    }

    public void delete(Integer id) {
        userDao.delete(id);
    }
    public User findone(Integer id){
        return userDao.findone(id);
    }

}
