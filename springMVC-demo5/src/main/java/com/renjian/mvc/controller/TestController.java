package com.renjian.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

//    ant风格的路径
    @RequestMapping("/**/testInterceptor")
    public String testInterceptor(){

        return "success";
    }

    @RequestMapping("/testExceptionHandler")
    public String testException(){

        System.out.println(2/0);
        return "success";
    }
}
