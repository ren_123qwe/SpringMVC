package com.renjian.mvc.controller;

import com.renjian.mvc.bean.User;

import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class HtttpController {

    @RequestMapping(value = "/testRequestBody",method = {RequestMethod.POST})
    public String testRequestbody(@RequestBody String handIInfo, RequestEntity<String> requestEntity){
        System.out.println("请求报文的信息：  "+handIInfo);
        System.out.println("请求头信息:  "+requestEntity.getHeaders());
        System.out.println("请求体信息：   "+requestEntity.getBody());
        return "success";
    }

    @RequestMapping("/testResponse")
    public void testResponse(HttpServletResponse response) throws IOException {
        response.getWriter().write("hello Response");
    }

//    此时方法的返回值会直接作为响应到浏览器的数据
    @RequestMapping("/testResponseBody")
    @ResponseBody
    public String testResponseBody(){

        return "success!!!!";
    }

//    测试响应值不是String类型，而是对象
    @RequestMapping("/testResponseUser")
    @ResponseBody
    public User testResponseUser(){

        return new User(1001,"jack","123456",20,"男");
    }

    @RequestMapping(value = "/testAjax",method = {RequestMethod.PUT})
    @ResponseBody
    public String testAxios(String username,String password){

        System.out.println(username+":"+password);
        return "hello,axios";
    }

}
