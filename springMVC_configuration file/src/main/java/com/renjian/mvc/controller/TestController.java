package com.renjian.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {

    @RequestMapping("/success")
    public String success(){
        System.out.println(5/0);
        return "success";
    }
}
