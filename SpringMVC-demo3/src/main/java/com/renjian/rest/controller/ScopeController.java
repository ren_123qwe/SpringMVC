package com.renjian.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
public class ScopeController {

//    使用servletAPI向request域对象共享数据
    @RequestMapping("/testRequestByServletAPI")
    public String testRequestByServletAPI(HttpServletRequest request){
        request.setAttribute("testRequestScope","helloServletAPI");
        return "success";
    }

    @RequestMapping("/testModelAndView")
    public ModelAndView testModelAndView(){

        ModelAndView modelAndView = new ModelAndView();
        //处理模型数据：即向请求域中共享数据
        modelAndView.addObject("testRequestScope","hello,ModelAndView");
        //设置视图名称,即目标页面的名称，方便thymeleaf进行渲染
        modelAndView.setViewName("success");
        System.out.println(modelAndView.getClass().getName());
        return modelAndView;
    }
    @RequestMapping("/testModel")
    public String testModel(Model model){
        model.addAttribute("testModelScope","hello,Model");
        System.out.println(model.getClass().getName());
        return "success";

    }

    @RequestMapping("/testMap")
    public String testMap(Map<String,Object> map){
        map.put("testMapScope","hello,Map");
        return "success";
    }

    @RequestMapping("/testModelMap")
    public String testModelMap(ModelMap modelMap){
        modelMap.addAttribute("testModelMapScope","hello,ModelMap");
        System.out.println(modelMap.getClass().getName());
        return "success";
    }
    @RequestMapping("/tsetSession")
    public String testServletAPISession(HttpSession session){
        session.setAttribute("testServletAPISession","hello,session");
        return "success";
    }
    @RequestMapping("/testApplication")
    public String testApplication(HttpSession session){
        ServletContext servletContext = session.getServletContext();
        servletContext.setAttribute("testApplicationScope","hello,Applications");
        return "success";
    }
}
