package com.renjian.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {
    @RequestMapping("/testThymeleafView")
    public String testThymeleafView(){

        return "success";
    }

    @RequestMapping("/testForward")
    public String testForward(){

//        forward为转发前缀，不会在使用Thymeleaf进行解析，
//        而DispatcherServlet前端控制器进行解析时，直接将前缀截掉，进行转发，也就到了/testThymeleafView所示的页面
        return "forward:/testThymeleafView";
    }

    @RequestMapping("/testRedirect")
    public String testRedirect(){
        return "redirect:/testThymeleafView";

    }
}
