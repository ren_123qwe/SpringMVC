package com.renjian.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    /**
     * 使用RESTful来模拟用户的增删改查
     * /users   GET     查询所有用户信息
     * /user/1   GET     根据用户id查询用户信息
     * /user   POST     添加用户信息
     * /user/1   DELETE     根据id删除用户信息
     * /user   POST     修改用户信息
     */

    @RequestMapping(value = "/users",method = {RequestMethod.GET})
    public String getAllUser(){
        System.out.println("查询所有用户信息");
        return "success";
    }

    @RequestMapping(value = "/user/{id}",method = {RequestMethod.GET})
    public String getUserByID(){

        return "success";
    }

    @RequestMapping(value = "/user",method = {RequestMethod.POST})
    public String addUser(String username,String password){
        System.out.println("实体类对象: "+username+"::"+password);


        return "success";
    }

    @RequestMapping(value = "/user",method = {RequestMethod.PUT})
    public String updateUser(){
        System.out.println("修改用户信息！！！！");
        return "success";
    }


}
