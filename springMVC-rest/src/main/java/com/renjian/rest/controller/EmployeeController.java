package com.renjian.rest.controller;

import com.renjian.rest.bean.Employee;
import com.renjian.rest.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@Controller
public class EmployeeController {


    @Autowired
    private EmployeeDao employeeDao;

    @GetMapping(value = "/employee")
    public String allEmployee(Model model){
        Collection<Employee> all = employeeDao.getAll();
        model.addAttribute("employeeList",all);
        return "employee_list";
    }

    @RequestMapping(value = "/employee/{id}",method = {RequestMethod.DELETE})
    public String deleteEmployee(@PathVariable Integer id){
        employeeDao.delete(id);
//        操作成功后与之前的请求没有关系了，所以要用重定向
//        否则，地址栏还是带参数的地址
        return "redirect:/employee";
    }

    @RequestMapping(value = "/employee",method = {RequestMethod.POST})
    public String addEmploy(Employee employee){

        employeeDao.save(employee);
        return "redirect:/employee";
    }

    @RequestMapping(value = "/employee/{id}",method = {RequestMethod.GET})
    public String getEmployeeByID(@PathVariable Integer id,Model model){
        Employee employee = employeeDao.get(id);
        model.addAttribute("employee",employee);
        return "employee_update";
    }
    @RequestMapping(value = "/employee",method = {RequestMethod.PUT})
    public String updateEmployee(Employee employee){
        System.out.println(employee.getId());
        System.out.println(employee.getGender());
        employeeDao.save(employee);
        return "redirect:/employee";
    }

}
